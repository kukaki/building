﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enum用の便利クラス
/// </summary>
public static class EnumUtility
{
    /// <summary>
    /// enumの中身数
    /// </summary>
    /// <typeparam name="T">enum名</typeparam>
    /// <returns></returns>
    public static int EnumLenght<T>() where T : struct
    {
        return Enum.GetValues(typeof(T)).Length;
    }

    /// <summary>
    /// enumの中身をランダムに取得
    /// </summary>
    /// <typeparam name="T">enum名</typeparam>
    /// <returns></returns>
    public static T EnumRandom<T>() where T : struct
    {
        int num = UnityEngine.Random.Range(0, EnumLenght<T>());
        return NumToEnum<T>(num);
    }

    /// <summary>
    /// 番号で指定されたenumを返す
    /// </summary>
    /// <typeparam name="T">enum名</typeparam>
    /// <param name="num">何番目か</param>
    /// <returns></returns>
    public static T NumToEnum<T>(int num) where T : struct
    {
        return (T)Enum.ToObject(typeof(T), num);
    }
}
