﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blockscript
{
    public class Stack : MonoBehaviour
    {
        [SerializeField] private GameObject block;


        void Start()
        {

        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0) && block)
            {
                MousePos();
            }
            else if (Input.GetMouseButtonDown(1))
            {
                MouseDelete();
            }
        }

        void MousePos()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Instantiate(block, hit.transform.position + hit.normal, Quaternion.identity);
            }
        }

        void MouseDelete()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Destroy(hit.transform.gameObject);
            }
        }
    }
}
