﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;

//制作者:野崎大智
namespace blockscript
{
    public class BluePrint : MonoBehaviour
    {
        //オブジェクトを宣言
        //public GameObject wallPrefab;

        //ランダム數値の制禦
        private int randomMin = 0;
        private int randomMax = 2;

        //生成スピード
        [SerializeField] float createSpeedMin;
        [SerializeField] float createSpeedMax;
        //配列の幅
        [SerializeField] int m_X; //x軸方向
        [SerializeField] int m_Y; //y軸方向
        [SerializeField] int m_Z; //Z軸方向

        //3次元の配列にする
        public BlockType[,,] map;

        //追加部分
        Block block;
        bool[,,] check;

        void Start()
        {
            block = GetComponent<Block>();
            check = new bool[m_X, m_Y, m_Z];
            map = new BlockType[m_X, m_Y, m_Z];
            //for文を用いて各インデックスに1もしくは0を代入
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    for (int k = 0; k < map.GetLength(2); k++)
                    {
                        map[i, j, k] = EnumUtility.EnumRandom<BlockType>(); //Random.Range(randomMin, randomMax);
                        check[i, j, k] = true;
                    }
                }
            }
            StartCoroutine(Plan());
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                // 現在のScene名を取得する
                Scene loadScene = SceneManager.GetActiveScene();
                // Sceneの読み直し
                SceneManager.LoadScene(loadScene.name);
            }
        }
        private IEnumerator Plan()
        {
            //各インデックスに代入された値を基に、壁の生成、不生成を判別
            for (int i = 0; i < map.GetLength(0) * map.GetLength(1) * map.GetLength(2); i++)
            {
                (int x, int y, int z) = CheckBlock();
                //インデックスの値が1の時、wallPrefabを生成
                if (map[x, y, z] != BlockType.none)
                {
                    GameObject go = Instantiate(block.BlockGet(map[x, y, z]));
                    go.transform.position = new Vector3(x, y, z);
                    yield return new WaitForSeconds(Random.Range(createSpeedMin, createSpeedMax));
                }
            }
        }

        //追加部分
        /// <summary>
        /// map内のまだ確認してないとこを返す
        /// </summary>
        /// <returns>(X座標,　Y座標,　Z座標)</returns>
        (int, int, int) CheckBlock()
        {
            int x = Random.Range(0, m_X);
            int y = Random.Range(0, m_Y);
            int z = Random.Range(0, m_Z);
            if (check[x, y, z])
            {
                check[x, y, z] = false;
                return (x, y, z);
            }
            else
            {
                for (int i = x; i < map.GetLength(0); i++)
                {
                    for (int j = y; j < map.GetLength(1); j++)
                    {
                        for (int k = z; k < map.GetLength(2); k++)
                        {
                            if (check[i, j, k])
                            {
                                check[i, j, k] = false;
                                return (i, j, k);
                            }
                        }
                    }
                }
                for (int i = x; i > 0; i--)
                {
                    for (int j = y; j > 0; j--)
                    {
                        for (int k = z; k > 0; k--)
                        {
                            if (check[i, j, k])
                            {
                                check[i, j, k] = false;
                                return (i, j, k);
                            }
                        }
                    }
                }
            }

            return (x, y, z);
        }
    }
}
