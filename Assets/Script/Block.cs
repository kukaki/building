﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blockscript
{
    //ブロックを増やす場合ここにも書き込む
    /// <summary>
    /// ブロックの種類
    /// </summary>
    public enum BlockType
    {
        /// <summary>
        /// 何もなし
        /// </summary>
        none,
        /// <summary>
        /// 赤いブロック
        /// </summary>
        redblock,
        /// <summary>
        /// 青いブロック
        /// </summary>
        blueblock,



    }

    public class Block : MonoBehaviour
    {
        /// <summary>
        /// Resourcesのブロックを入れる場所
        /// </summary>
        [SerializeField]private GameObject[] blocks = new GameObject[EnumUtility.EnumLenght<BlockType>()];

        /// <summary>
        /// 送られたenumと同じブロックをGameObjectとして返す
        /// </summary>
        /// <param name="blocktype">BlockType</param>
        /// <returns></returns>
        public GameObject BlockGet(BlockType blocktype)
        {
            return blocks[(int)blocktype];
        }

        /// <summary>
        /// Resetで事前に呼び出しておくとこ
        /// </summary>
        private void Reset()
        {
            //blocksにResourcesのblockフォルダにあるすべてを当てはめる
            GameObject[] Rblocks = Resources.LoadAll<GameObject>("block");
            for (int i = 0; i < blocks.Length; i++)
            {
                blocks[i] = Rblocks[i];
            }
        }
    }
}